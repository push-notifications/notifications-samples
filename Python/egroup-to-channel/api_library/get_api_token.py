#!/usr/bin/env python
from auth_get_sso_cookie import cern_sso
import subprocess
import requests

AUTH_HOSTNAME = "auth.cern.ch"
AUTH_REALM = "cern"

################# CONFIGURATION ##################

# The Client application (application portal, option my app cannot keep a secret)
#clientapp_name = "tmp-push-notifications-clientscript"
clientapp_name = "notifications-dev-clientapi"
# clientapp_name = "notifications-qa-clientapi"
# clientapp_name = "notifications-clientapi"

# Standard localhost uri for this virtual app
clientapp_uri = "https://localhost"

# The target application (the backend API), with granted permissions to client application for token exchange
#audience = "tmp-push-notifications"
audience = "notifications-dev"
# audience = "notifications-qa"
# audience = "notifications"

##################################################

#if __name__ == "__main__":
def get_api_token():
    # Get Token for the clientscript application
    # Using https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie/
    # Run with parameters for the clientscript application 
    # clientapi.py -u https://localhost -c tmp-push-notifications-clientscript
    #token = command_line_tools.auth_get_sso_token()
    # proc = subprocess.Popen(
    #     ["auth-get-sso-token", "-u", clientapp_uri, "-c", clientapp_name], 
    #     stdout=subprocess.PIPE, 
    #     stderr=subprocess.STDOUT)
    # token = proc.communicate()[0].rstrip()
    token = cern_sso.get_sso_token(clientapp_uri, clientapp_name, True, AUTH_HOSTNAME, AUTH_REALM)
    #print("TOKEN to exchange retrieved")
    #print(token)

    # Do Token Exchange for the Backend API application
    # https://auth.docs.cern.ch/user-documentation/oidc/exchange-for-api/
    r = requests.post(
            "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token",
            data={
                "client_id": clientapp_name,
                "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange",
                "subject_token": token,
                "requested_token_type": "urn:ietf:params:oauth:token-type:refresh_token",
                "audience": audience,
            },
        )
    if not r.ok:
        print(
            "The token response was not successful: {}".format(r.json()))
        r.raise_for_status()

    token_response = r.json()
    access_token = token_response["access_token"]
    #print("access_token retrieved")
    #print(access_token)
    return access_token

    # Then calls to the backend can be performed with this access token
    # ACCESS_TOKEN=$(python get-api-token.py)
    # curl -X GET "https://api-notifications-dev.app.cern.ch/channels/" -H  "authorization: Bearer $ACCESS_TOKEN"

