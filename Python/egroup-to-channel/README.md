# egroup-to-channel Python script

Tool to migrate egroups to Notification service Channels.

## Prerequisites for LXPLUS usage
Install a local auth-get-sso-cookie compatible with python3
```
git clone https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie.git
cd auth-get-sso-cookie
python3 setup.py install --user
```

Install a local suds module
```
pip3 install --user suds-py3
```

## Create a Channel for the specified Egroup
- With the corresponding grappa group as channel member
- With email posting permissions for egroup

Steps:
- Needs https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie/
    - Works on LXPLUS for example
- Edit ```clientapp_name``` and ```audience``` in ```get_api_token.py``` depending on your target (dev, qa, prod)

Samples:
- Migrate one egroup retrieving information from xldap: ```./egroup_to_channel.py -g it-dep-cda-wf -ldap```
- Migrate one egroup specifying information: ```./egroup_to_channel.py -g it-dep-cda-wf -a it-dep-cda-wf-admins -o awagner -d "All members of IT-CDA-WF"```

## TEMPORARY IMPLEMENTATION
Command line: ```--removeSync```

Script will for now complete migration by:
- Disable sync between Grappa group and Egroup
- Remove all EGroup members
- Add channel email to the EGroup members

As a result: all mails sent to the egroup will then be relayued to the channel which will handled delivery to the Grappa group members.

**Note: the egroup will be empty, so authorizations based on it will not work anymore.**

## OR Disable Egroup posting to Exchange
**Currently only running manually by Mail team, only for groups with no recursive membership**
The optimum migration should then be complete by:

Run Mail team (sympa) egroup migration script to:
- Stop sending mails to all egroup members via Exchange
- Relay the egroup mails to the Notification Channel email address