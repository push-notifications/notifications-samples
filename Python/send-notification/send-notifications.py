import requests

# Target Notification channel
notifications_sendapi = "https://notifications.web.cern.ch/api/notifications"
notifications_bearer  = "fill-me with Channel API Key"
channelId = "fill-me with ChannelID"

header = {'Authorization': 'Bearer ' + notifications_bearer}

# Notification json sample
# {
#  "notification": {
#    "target": "...",
#    "summary": "Notification title",
#    "priority": "LOW|NORMAL|IMPORTANT",
#    "body": "Notification content",
#     "link": if any,
#     "imgurl": if any,
#  }
# }

# Build json and post to notifications service
message = {
    "notification": {
        "body": "<p>Message content</p>",
        "summary": "This is a test notification",
        "target": channelId,
        "priority": 'NORMAL',
        "link": "any alternate url you might want to provide",
    }
}
#print(message)
response = requests.post(notifications_sendapi, json=message, headers=header)
#print(response)
#print(response.json())
