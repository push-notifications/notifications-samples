# Target Notification channel
$notifications_sendapi = "https://notifications.web.cern.ch/api/notifications"
$notifications_bearer  = "......."
$channelId = "......"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$Notification = @{ target=$channelId; summary='Title'; priority='NORMAL'; body='description' }
$Payload = @{ notification=$Notification}
$headers = @{Authorization = "Bearer $notifications_bearer"}
Invoke-RestMethod -Uri $notifications_sendapi -Method Post -ContentType 'application/json;charset=utf-8' -Headers $headers -Body (ConvertTo-Json $Payload)