# Powershell scripts
Use corresponding base uri depending on the target infrastructure:
- https://notifications.web.cern.ch/api
- https://notifications-qa.web.cern.ch/api
- https://notifications-dev.web.cern.ch/api

## Handling Keycloak SSO with Powershell
- Auth will require kerberos
- Account to be used needs to have accepted consent, use it once with a browser to check
- Sample code:
```
Add-Type -AssemblyName System.Web
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$AuthServer = "https://auth.cern.ch"
# authenticate with keycloak
# open page and let redirect to keycloak and create session cookies in bidule
$r = Invoke-Webrequest -uri $OneFeed.Uri -UserAgent "curl-sso-kerberos/CERN-SSO-Cookie-Powershell" -SessionVariable bidule -UseDefaultCredentials
# Extract the kerberos url
$kerberosRelUri = $r.Links | Where-Object { $_.id -eq "social-kerberos" } | Select-Object href
$kerberosUri = $AuthServer+[System.Web.HttpUtility]::htmlDecode($kerberosRelUri.href)
$tmppage = Invoke-Webrequest -uri $kerberosUri -UserAgent "curl-sso-kerberos/CERN-SSO-Cookie-Powershell" -WebSession $bidule -UseDefaultCredentials -MaximumRedirection 100
$wppage = Invoke-Webrequest -uri $OneFeed.Uri -WebSession $bidule
[xml]$Content = $wppage.Content
```
