[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Array of source RSS Feeds URIs and their sender name (optional)
$FeedList = @(
    [PSCustomObject]@{Sender="CERN Home News";Uri="https://home.cern/api/news/feed.rss";}
)

# Datafile used to remember which posts were already synced
# Full path is needed
$datafile = "C:\Program Files\CERN\RSS-To-Notifications\rss-to-notifications.dat"

# Target Notification channel
$notifications_sendapi = "https://notifications.web.cern.ch/api/notifications"
$notifications_bearer  = "fill-me with Channel API Key"
$channelId = "fill-me with ChannelID"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Processing all the source RSS
Foreach ($OneFeed in $FeedList)
{
    # Anonymous access
    # use read below to deal with accents, ok with Drupal
    [net.httpwebrequest]$httpwebrequest = [net.webrequest]::create($OneFeed.Uri)
    [net.httpWebResponse]$httpwebresponse = $httpwebrequest.getResponse()
    $reader = new-object IO.StreamReader($httpwebresponse.getResponseStream())
    [xml]$Content = $reader.ReadToEnd()
    $reader.Close()

    # Authenticate with OLD SSO (ADFS) using current kerberos token
    # $r = Invoke-Webrequest -uri $OneFeed.Uri -UserAgent "curl-sso-kerberos/CERN-SSO-Cookie-Powershell" -SessionVariable bidule -UseDefaultCredentials
    # try {
    #      $wppage = Invoke-Webrequest -uri $r.Forms[0].Action -Body $r.Forms[0] -Method 'POST' -SessionVariable bidule2 -ErrorAction Ignore | out-null
    # } catch {}
    # $cookies = $bidule2.Cookies.GetCookies($r.Forms[0].Action)
    # $wppage = Invoke-Webrequest -uri $OneFeed.Uri -WebSession $bidule2

    $Feed = $Content.rss.channel

    # No sorting, process latest first
    #ForEach ($msg in $Feed.Item)
    # Sort by pubDate, works for example on Drupal feeds
    $tmparray = $Feed.Item | Sort-Object { $_.pubDate -as [datetime] } 
    ForEach ($msg in $tmparray)
    {
        # Extract item guid to check if alreayd synced
        $guid = $msg.guid.'#text'
        if ($guid -eq $null) {
            $guid = $msg.guid
        }

        $alreadyposted = $null
        $alreadyposted = Select-String $guid -path $datafile
        
        if ($alreadyposted -eq $null)
        {
            "Posting message $guid"
            # Notification json sample
            # {
            #  "notification": {
            #    "target": "...",
            #    "summary": "Notification title",
            #    "priority": "LOW|NORMAL|IMPORTANT",
            #    "body": "Notification content",
            #     "link": if any,
            #     "imgurl": if any,
            #  }
            # }

            # One can try to extract a feature image and fill it's url in imgurl property
            # But the image must be accessible anonymously to render fine in the notification list

            # Build json and post to notifications service
            # body comes from description field, on some RSS like Wordpress description is a summary, fully body is body=$msg.encoded.InnerText
            $Notification = @{ target=$channelId; summary=$msg.Title; priority='NORMAL'; body=$msg.description; link=$msg.link; }
            $Payload = @{ notification=$Notification}
            $headers = @{Authorization = "Bearer $notifications_bearer"}
            Invoke-RestMethod -Uri $notifications_sendapi -Method Post -ContentType 'application/json;charset=utf-8' -Headers $headers -Body (ConvertTo-Json $Payload)

            # Saves which entries are posted
            Add-Content $datafile $guid
        } 
        else
        {
            "Already posted."
        }
    }

}

