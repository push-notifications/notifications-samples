# send-notification shell script
- Fill ```channelId``` in ```notification.json``` with the Channel ID
- Fill ```bearer``` in ```sendnotification.sh``` with the Channel API Key

Use corresponding base uri depending on the target infrastructure:
- https://notifications.web.cern.ch/api
- https://notifications-qa.web.cern.ch/api
- https://notifications-dev.web.cern.ch/api
