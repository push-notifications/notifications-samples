#!/bin/bash

curl -X POST https://notifications.web.cern.ch/api/notifications \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer fill-me-with-APIKey" \
    --data '@notification.json'


